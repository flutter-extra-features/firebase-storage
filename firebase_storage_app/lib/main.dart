import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:firebase_storage_app/firebase_options.dart';

import 'package:firebase_storage_app/pages/home_page.dart';
import 'package:firebase_storage_app/utils.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  await signInUserAnon();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const HomePage(),
    );
  }
}

/*
  Packages Used:-
    1] flutter pub add firebase_core    ==> A Flutter plugin to use the Firebase Core API, which enables connecting to multiple Firebase apps.
    2] flutter pub add firebase_auth    ==> A Flutter plugin to use the Firebase Authentication API.
    3] flutter pub add firebase_storage ==> A Flutter plugin to use the Firebase Cloud Storage API.
    4] flutter pub add gallery_picker   ==> Gallery Picker is a flutter package that will allow you to pick media file(s), manage and navigate inside your gallery with modern tools and views.

  
  Added following lines in AndroidManifest.xml for gallery picker

    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
    <uses-permission android:name="android.permission.READ_MEDIA_IMAGES"/>
    <uses-permission android:name="android.permission.READ_MEDIA_VIDEO"/>

  For firbase side change minSdkVersion to 21
*/